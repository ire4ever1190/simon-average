var guid = null;
var seqId = null;
 // This script can only run on certain pages so this just reduces the 
// hard coded variables

function calc_average(scores){
	var sum = 0;
	scores.forEach(function(x){sum += parseInt(x)});
	var average = sum/scores.length;
	average = Math.round(average * 100) / 100;
	return average
}

var base_url = window.location.origin + "/"
function request(url, data = null) {
    function reqListener() {
        console.log("Got response ${this.response}");
        // Does work on response depending on what information has 
       // already been got
        if (window.guid === null) {
            var re = /GUID=(.*)/;
            var tt = this.response['d']['UserPhotoUrl'];
            console.log(tt);
            window.guid = re.exec(tt)[1];
            console.log(guid);
            request(base_url + "WebModules/Profiles/Student/LearningResources/LearningAreaTasks.aspx/getSemesters", data=JSON.stringify({"guidString": guid.toString()}));
            return 1
        } else if (window.seqId === null){
            window.seqId = this.response['d'][0]['FileSeq'];
            console.log(seqId);
            request(base_url + "WebModules/Profiles/Student/LearningResources/LearningAreaTasks.aspx/getClasses", JSON.stringify({"guidString": window.guid.toString(), "fileSeq": seqId}));
            return 1
        }

        else {
            var score = 0;
            var scores = new Array();
            var find_score = /(\d?\d?\d)+%/;
	    var class_scores = {};
	    // Loops over all subjects and then all tasks in those 
           // subjects
            this.response['d']['SubjectClasses'].forEach(function (subject) {
				var temp_scores = new Array();
                subject['Tasks'].forEach(function (task) {
                    try {
			var score = find_score.exec(task['FinalResult'])[1];
			temp_scores.push(score);
                        scores.push(score);
                    } catch (e) {}
                })
			class_scores[subject["Code"]] = temp_scores;

            });
            console.log(score);
	    console.log(class_scores);
            var average = calc_average(scores);
            var ele = document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_WelcomeGreeting");
            ele.innerHTML = ele.innerHTML + "<br> Average: " + average.toString();
	    var class_elements = document.getElementsByClassName("TimetabledClassesText")
	    for (var key in class_scores){
	        if (class_scores[key].length !== 0) {
	            for (var x = 0; x < class_elements.length; x++){
			if (class_elements[x].innerHTML.includes(key)){
			    score = 0
			    class_elements[x].innerHTML = class_elements[x].innerHTML + " Average: " + calc_average(class_scores[key])
			}
		    }
	        }
            }
         }
    }
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", reqListener);
    oReq.open("POST", url, true);
    oReq.responseType = 'json';
    // These headers are needed
    oReq.setRequestHeader("Accept", "application/json, text/javascript, */*; q=0.01");
    oReq.setRequestHeader("Accept-Language", "en-GB,en;q=0.5");
    oReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    oReq.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    //Sends all cookies
    oReq.withCredentials = true;
    // This just sends data if it is actually provided
    if (data !== null){
        oReq.send(data);
    }else {
	oReq.send();
    }
}
if (document.getElementById("workdesk") !== null) {
	request(base_url + "Default.asmx/GetUserInfo?" + new Date().getTime());

}
