var guid = null;
var seqId = null;
function request(method, url, data = null) {
    function reqListener() {
        console.log(this.response);
        // This is a crude hack
        // What have I brought apon this land
        if (window.guid === null) {
            var re = /GUID=(.*)/;
            var tt = this.response['d']['UserPhotoUrl'];
            console.log(tt);
            window.guid = re.exec(tt)[1];
            console.log(guid);
            request("POST", "https://intranet.stpats.vic.edu.au/WebModules/Profiles/Student/LearningResources/LearningAreaTasks.aspx/getSemesters", data=JSON.stringify({"guidString": guid.toString()}));
            return 1
        } else if (seqId === null){
            window.seqId = this.response['d'][0]['FileSeq'];
            console.log(seqId);
            request("POST", "https://intranet.stpats.vic.edu.au/WebModules/Profiles/Student/LearningResources/LearningAreaTasks.aspx/getClasses", JSON.stringify({"guidString": window.guid.toString(), "fileSeq": seqId}));
            return 1
        }

        else {
            console.log("average found");
            console.log(this.response);
            var score = 0;
            var scores = new Array();
            var find_score = /(\d?\d?\d)+%/;
            this.response['d']['SubjectClasses'].forEach(function (subject) {
                subject['Tasks'].forEach(function (task) {
                    try {
                        scores.push(find_score.exec(task['FinalResult'])[1])
                    } catch (e) {
                        // I legit don't know how to pass errors
                        1 + 1
                    }

                })

            });
            scores.forEach(function(x){score += parseInt(x)});
            console.log(score);
            var average = score/scores.length;
            average = Math.round(average * 100) / 100;
           // browser.tabs.executeScript({code: 'alert("Your average is: " + ' +  average + ');'});
            browser.tabs.executeScript({code : 'document.getElementById("ContentPlaceHolder1_ContentPlaceHolder1_WelcomeGreeting").innerHTML += "<br>" + "Average: "' + average + ';'});


        }
    }

    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", reqListener);
    oReq.open(method, url, true);
    oReq.responseType = 'json';
    //Fucking javascript
    oReq.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0");
    oReq.setRequestHeader("Accept", "application/json, text/javascript, */*; q=0.01");
    oReq.setRequestHeader("Accept-Language", "en-GB,en;q=0.5");
    oReq.setRequestHeader("Referer", "https://intranet.stpats.vic.edu.au/");
    oReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    oReq.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    oReq.setRequestHeader("DNT", "1");
    oReq.setRequestHeader("Connection", "keep-alive");

    oReq.withCredentials = true;
    if (data !== null){
        oReq.send(data);
    }else {
    oReq.send();
    }


}


function run() {
    console.log("running");
    request("POST", "https://intranet.stpats.vic.edu.au/Default.asmx/GetUserInfo?" + new Date().getTime());
}


chrome.browserAction.onClicked.addListener(function (tab) { //Fired when User Clicks ICON
    console.log(tab.url);
    if (tab.url === "https://intranet.stpats.vic.edu.au/") { // Inspect whether the place where user run matches with our list of URL
        run()

    }
});


